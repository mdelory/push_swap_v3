import sys
import pyglet
from pyglet.window import key

COL_STACK_1 = (80, 0, 0, 80, 0, 0, 120, 0, 0, 120, 0, 0)
COL_STACK_2 = (0, 0, 80, 0, 0, 80, 0, 0, 120, 0, 0, 120)

COL_OPS = (10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10)

COL_TIME = (255, 202, 58, 255, 202, 58, 255, 202, 58, 255, 202, 58)

class Psplayer:
	def __init__(self):
		self.last_tick = 0
		self.play = 0
		self.speed = 1.0
		self.ctn = True
		self.n_ops = 0
		self.n_values = 0
		self.step = 0
		self.steps = []
		self.ops = []
		self.a = Stack()
		self.b = Stack()
		self.stat_ops = dict.fromkeys(['sa', 'sb', 'ss', 'pa', 'pb', 'ra', 'rb', 'rr', 'rra', 'rrb', 'rrr'], 0)

	def load(self, values):
		self.a.load(values)
		self.n_values = len(values)

	def save_step(self):
		self.steps.append([self.a.get_val(), self.b.get_val()])

	def get_step(self):
		return self.steps[self.step]

	def forward(self, n):
		self.step = min(self.step + n, self.n_ops - 1)

	def backward(self, n):
		self.step = max(self.step - n, 0)

	def exec_op(self, op):
		if op == 'sa' or op == 'ss':
			self.a.swap()
		if op == 'sb' or op == 'ss':
			self.b.swap()
		if op == 'pa':
			self.a.push(self.b.pop())
		if op == 'pb':
			self.b.push(self.a.pop())
		if op == 'ra' or op == 'rr':
			self.a.rotate()
		if op == 'rb' or op == 'rr':
			self.b.rotate()
		if op == 'rra' or op == 'rrr':
			self.a.revrotate()
		if op == 'rrb' or op == 'rrr':
			self.b.revrotate()

	def process(self):
		for line in sys.stdin:
			line = line.rstrip()
			if line in self.stat_ops:
				self.ops.append(line)
				self.steps.append((self.a.get_val() ,self.b.get_val()))
				self.exec_op(line)
				self.stat_ops[line] += 1
			else:
				print('Bad: ' + line)
		self.steps.append((self.a.get_val() ,self.b.get_val()))
		self.n_ops = len(self.steps)
		return True

class Window(pyglet.window.Window):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.alive = 1
		self.time_drag = 0
		self.player = Psplayer()
		self.c_op = pyglet.text.Label('',
						font_size=24,
						x=self.width / 2, y=118,
						anchor_x='center', anchor_y='center')
		pyglet.gl.glClearColor(0.31, 0.31, 0.31, 1)

	def on_draw(self):
		self.render()

	def on_close(self):
		self.alive = 0

	def on_mouse_press(self, x, y, button, modifiers):
		if button == pyglet.window.mouse.LEFT and y > 30 and y < 50:
			self.time_drag = 1

	def on_mouse_release(self, x, y, button, modifiers):
		if button == pyglet.window.mouse.LEFT:
			self.time_drag = 0

	def on_mouse_drag(self, x, y, dx, dy, button, modifiers):
		if self.time_drag == 1:
			self.player.step = int(max(0, (self.player.n_ops - 1) * min(1, x / self.width)))


	def on_key_press(self, symbol, modifiers):
		if (symbol == key.RIGHT):
			self.player.forward(1)
		elif (symbol == key.LEFT):
			self.player.backward(1)
		elif (symbol == key.SPACE):
			self.player.play = not self.player.play

	def render(self):
		self.clear()
		self.render_stacks()
		self.render_ops()
		self.render_timeline()
		self.flip()

	def render_stacks(self):
		s = self.player.get_step()
		self.render_single_stack(s[0], (20, 170, 750, 700), COL_STACK_1)
		self.render_single_stack(s[1], (820, 170, 750, 700), COL_STACK_2)

	def render_single_stack(self, val, area, color):
		x, y, w, h = area[0], area[1], area[2], area[3]
		border = 30
		rx, ry, rw, rh = int(x), int(y), int(w - border), int(h)
		step_y = rh / self.player.n_values
		space = int ((step_y - 1) / 10)
		step_y -= space
		for v, s in val:
			pyglet.graphics.draw(4, pyglet.gl.GL_QUADS, ('v2f', [rx, ry, rx + (s * rw), ry, rx + (s * rw), ry + step_y, rx, ry + step_y]),
														('c3B', color))
			ry += (step_y + space)
		pyglet.graphics.draw(2, pyglet.gl.GL_LINES,("v2f", [x, y, x + w, y]))
		pyglet.graphics.draw(2, pyglet.gl.GL_LINES,("v2f", [x + w, y, x + w, y + h]))
		pyglet.graphics.draw(2, pyglet.gl.GL_LINES,("v2f", [x + w, y + h, x, y + h]))
		pyglet.graphics.draw(2, pyglet.gl.GL_LINES,("v2f", [x, y + h, x, y]))

	def render_ops(self):
		pyglet.graphics.draw(4, pyglet.gl.GL_QUADS, ('v2f', [0, 80, self.width, 80, self.width, 140, 0, 140]),
													('c3b', COL_OPS))
		if (self.player.step < len(self.player.ops)):
			self.c_op.text = self.player.ops[self.player.step]
		else:
			self.c_op.text = '...'
		self.c_op.draw()

	def render_timeline(self):
		pyglet.graphics.draw(4, pyglet.gl.GL_QUADS, ('v2f', [0, 30, self.width, 30, self.width, 50, 0, 50]),
													('c3b', COL_OPS))
		cur = self.width * (self.player.step / (self.player.n_ops - 1)) - 5
		pyglet.graphics.draw(4, pyglet.gl.GL_QUADS, ('v2f', [cur, 30, cur + 10, 30, cur + 10, 50, cur, 50]),
													('c3B', COL_TIME))

	def run(self):
		while self.alive == 1:
			self.render()
			if self.player.play == 1:
				self.player.forward(1)
			event = self.dispatch_events()

class Stack:
	def __init__(self):
		self.values = []

	def load(self, values):
		s_values = sorted(values)
		n = len(s_values)
		for v in values:
			val = (v, float((s_values.index(v) + 1) / n))
			self.values.append(val)
		print('Stack loaded ! Size: ' + str(n))

	def get_val(self):
		return list(self.values)

	def swap(self):
		self.values.insert(1, self.pop())

	def push(self, val):
		self.values.insert(0, val)

	def pop(self):
		return (self.values.pop(0))

	def rotate(self):
	        self.values.append(self.pop())

	def revrotate(self):
		self.push(self.values.pop())

i = []

sys.argv.pop(0)
for n in sys.argv[0].split():
    i.append(int(n))

win = Window(1600, 900, "Push Swap player")
win.player.load(i)
win.player.process()
win.run()
