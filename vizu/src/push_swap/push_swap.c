/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/30 17:08:38 by mdelory           #+#    #+#             */
/*   Updated: 2019/08/22 15:56:04 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static void		free_ops(t_op *op_lst)
{
	t_op		*op;

	while (op_lst)
	{
		op = op_lst;
		op_lst = op_lst->next;
		free(op->op);
		free(op);
	}
}

static void		print_ops(t_op *op_lst)
{
	while (op_lst)
	{
		ft_putendl(op_lst->op);
		op_lst = op_lst->next;
	}
	free_ops(op_lst);
}

int				main(int ac, char **av)
{
	long		*s_val;
	t_stack		a;
	t_stack		b;
	t_op		*op_lst;

	if (ac < 2)
		return (1);
	st_init(&a, &b);
	op_lst = NULL;
	if (stack_load(&a, ac, av))
	{
		if ((s_val = get_sorted_values(&a)))
		{
			index_values(&a, s_val);
			a.index = s_val;
			a.last = &s_val[a.size - 1];
			main_algo(&a, &b, a.size, &op_lst);
			free(s_val);
		}
	}
	optimize(&op_lst);
	print_ops(op_lst);
	st_free(&a);
	st_free(&b);
	//while (1);
	return (0);
}
