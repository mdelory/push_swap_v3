/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   values.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/22 12:51:48 by mdelory           #+#    #+#             */
/*   Updated: 2019/08/22 13:02:11 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static long		*get_values(t_stack *s)
{
	t_ste		*ste;
	long		*tab;
	long		i;

	if ((tab = (long *)malloc(sizeof(long) * s->size)))
	{
		i = 0;
		ste = s->p;
		while (i < s->size)
		{
			tab[i] = ste->nbr;
			ste = ste->next;
			i++;
		}
	}
	return (tab);
}

static void		index_val(t_stack *s, t_ste *p, long *tab)
{
	long		i;

	i = 0;
	while (i <= s->size)
	{
		if (p->nbr == tab[i])
		{
			p->pos = &tab[i];
			return ;
		}
		i++;
	}
}

void			index_values(t_stack *s, long *tab)
{
	t_ste		*ste;
	long		i;

	i = 0;
	ste = s->p;
	while (i <= s->size)
	{
		index_val(s, ste, tab);
		ste = ste->next;
		i++;
	}
}

long			*get_sorted_values(t_stack *s)
{
	long		*tab;
	long		tmp;
	long		i;
	long		j;

	if ((tab = get_values(s)))
	{
		i = 0;
		while (i < s->size - 1)
		{
			j = 0;
			while (j < s->size - i - 1)
			{
				if (tab[j] > tab[j + 1])
				{
					tmp = tab[j];
					tab[j] = tab[j + 1];
					tab[j + 1] = tmp;
				}
				j++;
			}
			i++;
		}
	}
	return (tab);
}
