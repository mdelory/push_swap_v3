/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_rotate.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 16:26:28 by mdelory           #+#    #+#             */
/*   Updated: 2019/08/22 10:58:17 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static int		rotate(t_stack *s)
{
	if (s->size > 1)
		s->p = s->p->next;
	return (1);
}

int				op_rotate(char sel, t_stack *a, t_stack *b)
{
	int			ret;

	ret = 0;
	if (sel == 'a' || sel == 'r')
		ret = rotate(a);
	if (sel == 'b' || sel == 'r')
		ret = rotate(b);
	return (ret);
}
