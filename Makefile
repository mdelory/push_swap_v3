# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mdelory <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/16 20:47:09 by mdelory           #+#    #+#              #
#    Updated: 2019/08/22 13:00:36 by mdelory          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME_CHECKER = 		checker
NAME_PUSH_SWAP = 	push_swap

SRC_STACK =			src/stack/stack.c \
					src/stack/op_swap.c \
					src/stack/op_push.c \
					src/stack/op_rotate.c \
					src/stack/op_revrotate.c \
					src/stack/stack_load.c \
					src/stack/stack_exec.c \

SRC_CHECKER =		$(SRC_STACK) \
					src/checker/checker.c

SRC_PUSH_SWAP =		$(SRC_STACK) \
					src/push_swap/push_swap.c \
					src/push_swap/values.c \
					src/optimize/optimize.c \
					src/algo/main_algo.c \
					src/algo/sort.c 

OBJ_PUSH_SWAP = 	$(SRC_PUSH_SWAP:.c=.o)
OBJ_CHECKER = 		$(SRC_CHECKER:.c=.o)

INC =				-Iincludes/ -Ilibft/includes/
LDFLAGS +=			-Llibft -lft
CFLAGS +=			-Werror -Wall -Wextra

CC =				clang
RM =				/bin/rm -f

################################################################################
################################################################################

all: 					$(NAME_CHECKER) $(NAME_PUSH_SWAP)

%.o:			%.c
	@echo "- Compiling $(NAME): $^"
	@$(CC) $(CFLAGS) $(INC) -g -c -o $@ $^

$(NAME_CHECKER):		$(OBJ_CHECKER)
	@$(MAKE) -C libft
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(NAME_CHECKER) $(OBJ_CHECKER)

$(NAME_PUSH_SWAP):		$(OBJ_PUSH_SWAP)
	@$(MAKE) -C libft
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(NAME_PUSH_SWAP) $(OBJ_PUSH_SWAP)

.phony: clean
clean:
	@$(MAKE) -C libft clean
	$(RM) $(OBJ_CHECKER) $(OBJ_PUSH_SWAP)

.phony: fclean
fclean:			 		clean
	@$(MAKE) -C libft fclean
	@$(RM) $(NAME_CHECKER) $(NAME_PUSH_SWAP)

.phony: re
re: fclean all
