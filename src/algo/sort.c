/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/06 15:40:53 by mdelory           #+#    #+#             */
/*   Updated: 2019/08/22 13:05:38 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

int				add_list(t_stack *a, int size)
{
	int			i;

	i = -1;
	if (a->index == a->last)
		return (0);
	while (++i < size)
	{
		if (a->index == a->last)
			return (0);
		a->sort += 1;
		a->index += 1;
	}
	return (1);
}

void			check_bfirst(t_stack *a, t_stack *b, t_op **op_lst)
{
	if (b->p->pos == a->index || b->p->next->pos == a->index)
	{
		if (b->p->pos == a->index && b->p->next->pos == (a->index + 1))
		{
			st_exec("pa", a, b, op_lst);
			st_exec("ra", a, b, op_lst);
			st_exec("pa", a, b, op_lst);
			st_exec("ra", a, b, op_lst);
			add_list(a, 2);
		}
		else if (b->p->next->pos == a->index && b->p->pos == (a->index + 1))
		{
			st_exec("rb", a, b, op_lst);
			st_exec("pa", a, b, op_lst);
			st_exec("ra", a, b, op_lst);
			st_exec("pa", a, b, op_lst);
			st_exec("ra", a, b, op_lst);
			add_list(a, 2);
		}
	}
}

void			check_afirst(t_stack *a, t_stack *b, t_op **op_lst)
{
	if (a->p->pos == a->index || a->p->next->pos == a->index)
	{
		if (a->p->pos == a->index && a->p->next->pos == (a->index + 1))
		{
			st_exec("ra", a, b, op_lst);
			st_exec("ra", a, b, op_lst);
			add_list(a, 2);
		}
		else if (a->p->next->pos == a->index && a->p->pos == (a->index + 1))
		{
			st_exec("sa", a, b, op_lst);
			st_exec("ra", a, b, op_lst);
			st_exec("ra", a, b, op_lst);
			add_list(a, 2);
		}
	}
}

void			ping_bpush(t_stack *a, t_stack *b, t_op **op_lst)
{
	int			i;

	i = -1;
	while (++i < 3)
	{
		if (b->p->nbr == *(a->index))
		{
			add_list(a, 1);
			st_exec("pa", a, b, op_lst);
			st_exec("ra", a, b, op_lst);
			check_bfirst(a, b, op_lst);
			return ;
		}
		st_exec("rb", a, b, op_lst);
	}
}

void			ping_apush(t_stack *a, t_stack *b, t_op **op_lst)
{
	if (a->p->pos - a->index == 2)
	{
		st_exec("pb", a, b, op_lst);
		check_afirst(a, b, op_lst);
		st_exec("pa", a, b, op_lst);
		if (a->p->pos == a->index)
		{
			add_list(a, 1);
			st_exec("ra", a, b, op_lst);
		}
	}
	else if (a->p->next->pos - a->index == 2)
	{
		if (a->p->pos == a->index)
		{
			add_list(a, 1);
			st_exec("ra", a, b, op_lst);
			check_afirst(a, b, op_lst);
		}
		else
		{
			st_exec("pb", a, b, op_lst);
			st_exec("pb", a, b, op_lst);
			if (a->p->pos == a->index)
			{
				add_list(a, 1);
				st_exec("ra", a, b, op_lst);
			}
			check_bfirst(a, b, op_lst);
		}
	}
	else
	{
		check_afirst(a, b, op_lst);
		if (a->p->pos == a->index)
		{
			add_list(a, 1);
			st_exec("ra", a, b, op_lst);
		}
	}
}

void			size_asort(t_stack *a, t_stack *b, long size, t_op **op_lst)
{
	if (size == 1 && a->p->nbr == *(a->index))
	{
		add_list(a, 1);
		st_exec("ra", a, b, op_lst);
	}
	else if (size == 2)
		check_afirst(a, b, op_lst);
	else if (size == 3)
		ping_apush(a, b, op_lst);
}

int				size_bsort(t_stack *a, t_stack *b, long size, t_op **op_lst)
{
	if (size == b->size)
	{
		if (size == 1 && b->p->nbr == *(a->index))
		{
			add_list(a, 1);
			st_exec("pa", a, b, op_lst);
			st_exec("ra", a, b, op_lst);
		}
		if (size == 2)
			check_bfirst(a, b, op_lst);
		if (size == 3)
			ping_bpush(a, b, op_lst);
		return (1);
	}
	else
		return (0);
}
