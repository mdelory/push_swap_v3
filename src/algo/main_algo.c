/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_algo.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/06 15:34:04 by mdelory           #+#    #+#             */
/*   Updated: 2019/08/22 14:03:10 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

long			div_count(long size)
{
	long	count;

	count = 1;
	while (size > 3)
	{
		size = (size / 2);
		count++;
	}
	return (count);
}

int				rrewind(t_stack *a, t_stack *b, t_op **op_lst)
{
	while (a->p->prev->nbr >= *(a->index))
		st_exec("rra", a, b, op_lst);
	return (0);
}

long			*get_size_tab(long size)
{
	long	*tab;
	long	count_div;

	count_div = div_count(size);
	if (!(tab = (long *)malloc(sizeof(long) * count_div + 1)))
		return (0);
	tab[count_div--] = 0;
	while (size > 3)
	{
		tab[count_div--] = (size / 2) + (size % 2);
		size /= 2;
	}
	if (size > 0)
		tab[count_div] = size;
	else
		return (NULL);
	return (tab);
}

static void		rb_split(t_stack *a, t_stack *b, long med, t_op **op_lst)
{
	long		half;

	half = (b->size / 2) + (b->size % 2);
	while (half)
	{
		if (b->p->nbr >= med)
		{
			st_exec("pa", a, b, op_lst);
			half--;
		}
		else
			st_exec("rb", a, b, op_lst);
	}
}

static int		rb_sort(t_stack *a, t_stack *b, t_op **op_lst)
{
	long		med;

	if (!b->size)
		return (0);
	while (b->size > 3)
	{
		med = *(a->index + (b->size / 2));
		rb_split(a, b, med, op_lst);
	}
	return (1);
}

static void		ra_split(t_stack *a, t_stack *b, long size, t_op **op_lst)
{
	long		med;
	long		half;

	half = size / 2;
	med = a->index[half];
	while (half)
	{
		if (a->p->nbr < med)
		{
			st_exec("pb", a, b, op_lst);
			half--;
		}
		else
			st_exec("ra", a, b, op_lst);
	}
}

int				main_algo(t_stack *a, t_stack *b, long size, t_op **op_lst)
{
	long		*size_tab;
	int			i;

	if (a->p->prev->nbr >= *(a->index) && a->sort)
		rrewind(a, b, op_lst);
	if ((i = -1) && size > 3)
	{
		ra_split(a, b, size, op_lst);
		if (!(size_tab = get_size_tab(size)))
			return (0);
		if (a->sort)
			rrewind(a, b, op_lst);
		if (rb_sort(a, b, op_lst))
			size_bsort(a, b, size_tab[++i], op_lst);
		while (size_tab[++i])
		{
			if (size_tab[i] <= 3)
				size_asort(a, b, size_tab[i], op_lst);
			else
				main_algo(a, b, size_tab[i], op_lst);
		}
		free(size_tab);
	}
	else if (size > 0)
		size_asort(a, b, size, op_lst);
	return (1);
}
