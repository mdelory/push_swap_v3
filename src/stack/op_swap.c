/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 16:22:54 by mdelory           #+#    #+#             */
/*   Updated: 2019/08/22 13:07:41 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static int		swap(t_stack *s)
{
	int			tmp;

	if (s->size > 2)
	{
		tmp = s->p->nbr;
		s->p->nbr = s->p->next->nbr;
		s->p->next->nbr = tmp;
	}
	return (1);
}

int				op_swap(char sel, t_stack *a, t_stack *b)
{
	int		ret;

	ret = 0;
	if (sel == 'a' || sel == 's')
		ret = swap(a);
	if (sel == 'b' || sel == 's')
		ret = swap(b);
	return (ret);
}
