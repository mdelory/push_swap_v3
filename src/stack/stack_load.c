/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_load.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/30 15:55:18 by mdelory           #+#    #+#             */
/*   Updated: 2019/08/22 10:59:12 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static int		check_av(char *av)
{
	char		*s;

	s = av;
	if (*av == '-' || *av == '+')
		av++;
	while (*av)
	{
		if (!ft_isdigit(*av))
		{
			ft_dprintf(2, "Error: not a number (%s)\n", s);
			return (0);
		}
		av++;
	}
	return (1);
}

static int		check_dupe(t_stack *s, int nbr)
{
	t_ste		*ste;
	long		i;

	if ((ste = s->p))
	{
		i = 0;
		while (i < s->size)
		{
			if (ste->nbr == nbr)
			{
				ft_dprintf(2, "Error: duplicated number (%d)\n", ste->nbr);
				return (0);
			}
			ste = ste->next;
			i++;
		}
	}
	return (1);
}

static int		load_av(t_stack *s, int ac, char **av)
{
	t_ste		*enew;
	int			nbr;
	int			ret;

	ret = 1;
	while (ret && ac >= 0)
	{
		if ((ret = (check_av(av[ac]))))
		{
			nbr = ft_atoi(av[ac]);
			if (check_dupe(s, nbr) && (enew = st_enew(nbr)))
				st_push(s, enew);
			else
				ret = 0;
		}
		ac--;
	}
	return (ret);
}

int				stack_load(t_stack *s, int ac, char **av)
{
	char		**tab;
	int			ret;
	int			i;

	ret = 1;
	while (ret && --ac)
	{
		if ((tab = ft_strsplit(av[ac], ' ')))
		{
			i = 0;
			while (tab[i])
				i++;
			ret = load_av(s, i - 1, tab);
			ft_freetab(tab);
		}
		else
			ret = 0;
	}
	return (ret);
}
