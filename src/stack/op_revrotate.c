/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_revrotate.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/06 18:07:27 by mdelory           #+#    #+#             */
/*   Updated: 2019/08/22 12:44:16 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "stack.h"

static int		revrotate(t_stack *s)
{
	if (s->size > 1)
		s->p = s->p->prev;
	return (1);
}

int				op_revrotate(char sel, t_stack *a, t_stack *b)
{
	int			ret;

	ret = 0;
	if (sel == 'a' || sel == 'r')
		ret = revrotate(a);
	if (sel == 'b' || sel == 'r')
		ret = revrotate(b);
	return (ret);
}
