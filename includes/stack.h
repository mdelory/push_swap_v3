/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/30 13:34:01 by mdelory           #+#    #+#             */
/*   Updated: 2019/08/22 12:54:51 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STACK_H
# define STACK_H
# include "libft.h"

# include <stdio.h>

typedef struct			s_stack
{
	long				size;
	long				sort;
	long				*index;
	long				*last;
	struct s_ste		*p;
}						t_stack;

typedef struct			s_ste
{
	long				nbr;
	long				*pos;
	struct s_ste		*next;
	struct s_ste		*prev;
}						t_ste;

typedef struct			s_op
{
	char				*op;
	struct s_op			*last;
	struct s_op			*next;
}						t_op;

void					st_init(t_stack *a, t_stack *b);
t_ste					*st_enew(long nbr);
void					st_push(t_stack *s, t_ste *enew);
t_ste					*st_pop(t_stack *s);
void					st_free(t_stack *s);
void					st_print(t_stack *s);

int						st_exec(char *op, t_stack *a, t_stack *b, \
									t_op **op_lst);

int						stack_load(t_stack *s, int ac, char **av);

int						op_push(char sel, t_stack *a, t_stack *b);
int						op_swap(char sel, t_stack *a, t_stack *b);
int						op_rotate(char sel, t_stack *a, t_stack *b);
int						op_revrotate(char sel, t_stack *a, t_stack *b);

void					optimize(t_op **op_lst);

int						main_algo(t_stack *a, t_stack *b, long size, \
									t_op **op_lst);
void					check_bfirst(t_stack *a, t_stack *b, t_op **op_lst);
void					size_asort(t_stack *a, t_stack *b, long size, \
									t_op **op_lst);
int						size_bsort(t_stack *a, t_stack *b, long size, \
									t_op **op_lst);
void					get_end(t_stack *a, t_stack *b, int size);

void					index_values(t_stack *s, long *tab);
long					*get_sorted_values(t_stack *s);
#endif
